use std::time::Duration;

use winit::{
    dpi::LogicalSize,
    event::{Event, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};

mod rendering;

fn main() {
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("trace")).init();

    let event_loop = EventLoop::new();

    let window = WindowBuilder::new()
        .with_title("2D Game Prototype")
        .with_inner_size(LogicalSize::new(1024, 768))
        .build(&event_loop)
        .expect("Failed to create the game window.");

    let mut renderer =
        rendering::Renderer::init(&window).expect("Failed to initialise the renderer.");

    let mut should_draw = true;

    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Poll;

        match event {
            Event::MainEventsCleared => {
                if should_draw {
                    renderer.draw_frame().expect("Failed to draw a frame.");
                }
            }
            Event::WindowEvent { event, .. } => {
                if event == WindowEvent::CloseRequested {
                    *control_flow = ControlFlow::Exit;
                    should_draw = false;
                    unsafe {
                        renderer
                            .device
                            .lock()
                            .unwrap()
                            .device_handle
                            .device_wait_idle()
                            .expect("Failed to wait for device idle.");
                    }
                }
            }
            _ => (),
        }
    })
}
