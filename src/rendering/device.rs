use std::{
    error::Error,
    ffi::CStr,
    sync::{Arc, Mutex},
};

use gpu_allocator::vulkan::{Allocator, AllocatorCreateDesc};

use super::{Instance, Surface};

pub struct Device {
    pub physical_device: ash::vk::PhysicalDevice,
    pub device_handle: ash::Device,

    pub graphics_queue: ash::vk::Queue,
    pub graphics_family_index: u32,

    // This needs to be wrapped in an Option so that it can be easily dropped.
    pub memory_allocator: Option<Arc<Mutex<Allocator>>>,
}

impl Device {
    pub fn new(instance: &Instance, surface: &Surface) -> Result<Self, Box<dyn Error>> {
        let physical_devices = unsafe { instance.instance_handle.enumerate_physical_devices()? };

        let required_extensions = [
            ash::extensions::khr::Swapchain::name(),
            ash::extensions::khr::DynamicRendering::name(),
            ash::extensions::khr::Synchronization2::name(),
        ];

        // The DeviceCreateInfo needs the extension names as a *const c_char slice.
        let enabled_extensions = required_extensions
            .iter()
            .map(|extension| extension.as_ptr())
            .collect::<Vec<_>>();

        let core_features = ash::vk::PhysicalDeviceFeatures::default();

        let mut dynamic_rendering_features =
            ash::vk::PhysicalDeviceDynamicRenderingFeaturesKHR::builder().dynamic_rendering(true);

        let mut synchronization2_features =
            ash::vk::PhysicalDeviceSynchronization2Features::builder().synchronization2(true);

        let mut required_features = ash::vk::PhysicalDeviceFeatures2::builder()
            .push_next(&mut dynamic_rendering_features)
            .push_next(&mut synchronization2_features)
            .features(core_features)
            .build();

        // Nothing fancy yet, just pick the first device (if any) that supports a graphics queue
        // family.
        let mut graphics_family_index: Option<usize> = None;
        let physical_device = physical_devices.iter().find_map(|&device| {
            graphics_family_index = unsafe {
                instance
                    .instance_handle
                    .get_physical_device_queue_family_properties(device)
                    .iter()
                    .enumerate()
                    .position(|(index, queue_family)| {
                        let supports_graphics = queue_family
                            .queue_flags
                            .contains(ash::vk::QueueFlags::GRAPHICS);

                        let supports_presentation = surface
                            .surface_loader
                            .get_physical_device_surface_support(
                                device,
                                index.try_into().unwrap(),
                                surface.surface_handle,
                            )
                            .expect("Failed to get physical device support for the surface.");

                        supports_graphics && supports_presentation
                    })
            };

            let supports_required_extensions = unsafe {
                let supported_extensions = instance
                    .instance_handle
                    .enumerate_device_extension_properties(device)
                    .unwrap_or_default();

                let supported_extension_names = supported_extensions
                    .iter()
                    .map(|extension| CStr::from_ptr(extension.extension_name.as_ptr()))
                    .collect::<Vec<_>>();

                required_extensions.iter().all(|required_extension| {
                    supported_extension_names.contains(required_extension)
                })
            };

            // TODO: Check that the required features are supported.
            if graphics_family_index.is_some() && supports_required_extensions {
                Some(device)
            } else {
                None
            }
        });

        if physical_device.is_none() {
            return Err(
                String::from("No suitable devices found. Initialisation can not continue").into(),
            );
        }

        let physical_device = physical_device.unwrap();
        let graphics_family_index = graphics_family_index.unwrap() as u32;

        // Only a single (graphics) queue is used for now, but there needs to be one of these per queue.
        let queue_infos = [ash::vk::DeviceQueueCreateInfo::builder()
            .queue_family_index(graphics_family_index)
            .queue_priorities(&[1.0])
            .build()];

        let device_info = ash::vk::DeviceCreateInfo::builder()
            .push_next(&mut required_features)
            .queue_create_infos(&queue_infos)
            .enabled_extension_names(&enabled_extensions);

        let device = unsafe {
            instance
                .instance_handle
                .create_device(physical_device, &device_info, None)?
        };

        let graphics_queue = unsafe { device.get_device_queue(graphics_family_index, 0) };

        let memory_allocator_info = AllocatorCreateDesc {
            instance: instance.instance_handle.clone(),
            physical_device: physical_device,
            device: device.clone(),
            debug_settings: Default::default(),
            buffer_device_address: false,
        };

        let memory_allocator = Allocator::new(&memory_allocator_info)?;

        Ok(Self {
            physical_device: physical_device,
            device_handle: device,

            graphics_queue: graphics_queue,
            graphics_family_index: graphics_family_index,

            memory_allocator: Some(Arc::new(Mutex::new(memory_allocator))),
        })
    }
}

impl Drop for Device {
    fn drop(&mut self) {
        unsafe {
            // The memory allocator needs to be dropped before the device is destroyed.
            std::mem::take(&mut self.memory_allocator);
            self.device_handle.destroy_device(None);
        }
    }
}
