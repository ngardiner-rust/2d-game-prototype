use std::{
    error::Error,
    sync::{Arc, Mutex},
};

use super::Device;

pub struct FrameResources {
    pub command_pool: ash::vk::CommandPool,
    pub command_buffer: ash::vk::CommandBuffer,

    pub image_acquired_semaphore: ash::vk::Semaphore,
    pub render_finished_semaphore: ash::vk::Semaphore,
    pub render_finished_fence: ash::vk::Fence,

    device: Arc<Mutex<Device>>,
}

impl FrameResources {
    pub fn new(device: Arc<Mutex<Device>>) -> Result<Self, Box<dyn Error>> {
        let command_pool_info = ash::vk::CommandPoolCreateInfo::builder()
            .queue_family_index(device.lock().unwrap().graphics_family_index)
            .flags(ash::vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER);

        let command_pool = unsafe {
            device
                .lock()
                .unwrap()
                .device_handle
                .create_command_pool(&command_pool_info, None)?
        };

        let command_buffer_info = ash::vk::CommandBufferAllocateInfo::builder()
            .command_pool(command_pool)
            .level(ash::vk::CommandBufferLevel::PRIMARY)
            .command_buffer_count(1);

        let command_buffer = unsafe {
            device
                .lock()
                .unwrap()
                .device_handle
                .allocate_command_buffers(&command_buffer_info)?[0]
        };

        let image_acquired_semaphore = unsafe {
            device
                .lock()
                .unwrap()
                .device_handle
                .create_semaphore(&ash::vk::SemaphoreCreateInfo::default(), None)?
        };

        let render_finished_semaphore = unsafe {
            device
                .lock()
                .unwrap()
                .device_handle
                .create_semaphore(&ash::vk::SemaphoreCreateInfo::default(), None)?
        };

        let render_finished_fence = unsafe {
            device.lock().unwrap().device_handle.create_fence(
                &ash::vk::FenceCreateInfo::builder().flags(ash::vk::FenceCreateFlags::SIGNALED),
                None,
            )?
        };

        Ok(Self {
            command_pool: command_pool,
            command_buffer: command_buffer,

            image_acquired_semaphore: image_acquired_semaphore,
            render_finished_semaphore: render_finished_semaphore,
            render_finished_fence: render_finished_fence,

            device: device,
        })
    }

    pub fn begin_command_buffer(
        &self,
        usage_flags: ash::vk::CommandBufferUsageFlags,
    ) -> Result<(), Box<dyn Error>> {
        unsafe {
            Ok(self
                .device
                .lock()
                .unwrap()
                .device_handle
                .begin_command_buffer(
                    self.command_buffer,
                    &ash::vk::CommandBufferBeginInfo::builder().flags(usage_flags),
                )?)
        }
    }

    pub fn end_command_buffer(&self) -> Result<(), Box<dyn Error>> {
        unsafe {
            Ok(self
                .device
                .lock()
                .unwrap()
                .device_handle
                .end_command_buffer(self.command_buffer)?)
        }
    }

    pub fn await_render_finished_fence(&self) -> Result<(), Box<dyn Error>> {
        unsafe {
            Ok(self.device.lock().unwrap().device_handle.wait_for_fences(
                &[self.render_finished_fence],
                true,
                std::u64::MAX,
            )?)
        }
    }

    pub fn reset_render_finished_fence(&self) -> Result<(), Box<dyn Error>> {
        unsafe {
            Ok(self
                .device
                .lock()
                .unwrap()
                .device_handle
                .reset_fences(&[self.render_finished_fence])?)
        }
    }

    pub fn reset_command_buffer(&self) -> Result<(), Box<dyn Error>> {
        unsafe {
            Ok(self
                .device
                .lock()
                .unwrap()
                .device_handle
                .reset_command_buffer(
                    self.command_buffer,
                    ash::vk::CommandBufferResetFlags::empty(),
                )?)
        }
    }
}

impl Drop for FrameResources {
    fn drop(&mut self) {
        unsafe {
            self.device
                .lock()
                .unwrap()
                .device_handle
                .destroy_semaphore(self.image_acquired_semaphore, None);

            self.device
                .lock()
                .unwrap()
                .device_handle
                .destroy_semaphore(self.render_finished_semaphore, None);

            self.device
                .lock()
                .unwrap()
                .device_handle
                .destroy_fence(self.render_finished_fence, None);

            self.device
                .lock()
                .unwrap()
                .device_handle
                .destroy_command_pool(self.command_pool, None);
        }
    }
}
