use std::{
    error::Error,
    ffi::{c_void, CStr, CString},
};

use winit::window::Window;

pub struct Instance {
    pub entry: ash::Entry,
    pub instance_handle: ash::Instance,

    pub debug_utils_loader: Option<ash::extensions::ext::DebugUtils>,
    pub debug_utils_messenger: Option<ash::vk::DebugUtilsMessengerEXT>,
}

impl Instance {
    pub fn new(window: &Window) -> Result<Self, Box<dyn Error>> {
        let entry = unsafe { ash::Entry::load()? };

        let application_name = CString::new("2D Game Prototype")?;

        let application_info = ash::vk::ApplicationInfo::builder()
            .application_name(&application_name)
            .application_version(ash::vk::make_api_version(0, 0, 0, 0))
            .api_version(ash::vk::make_api_version(0, 1, 3, 0))
            .engine_name(CStr::from_bytes_with_nul(b"Vulkan Engine\0")?)
            .engine_version(ash::vk::make_api_version(0, 0, 0, 0));

        let mut requested_layers = vec![
            #[cfg(debug_assertions)]
            CString::new("VK_LAYER_KHRONOS_validation")?,
            CString::new("VK_LAYER_MESA_overlay")?,
        ];

        // The InstanceCreateInfo needs the layer and extension names as a *const c_char slice.
        let enabled_layers = {
            let supported_layer_properties = entry.enumerate_instance_layer_properties()?;

            let supported_layer_names = supported_layer_properties
                .iter()
                .map(|supported_layer| unsafe {
                    CStr::from_ptr(supported_layer.layer_name.as_ptr())
                })
                .collect::<Vec<_>>();

            requested_layers.retain(|layer_name| {
                if supported_layer_names.contains(&layer_name.as_c_str()) {
                    true
                } else {
                    log::warn!(
                        "The requested layer {} is unsupported and will not be enabled.",
                        layer_name.to_str().unwrap()
                    );

                    false
                }
            });

            requested_layers
                .iter()
                .map(|layer| layer.as_ptr())
                .collect::<Vec<_>>()
        };

        let mut required_extensions = ash_window::enumerate_required_extensions(window)?
            .iter()
            .map(|&extension| unsafe { CStr::from_ptr(extension) })
            .collect::<Vec<_>>();

        required_extensions.append(&mut vec![
            // Required for usage of the debug messenger.
            #[cfg(debug_assertions)]
            ash::extensions::ext::DebugUtils::name(),
            // Required for usage of dynamic rendering.
            ash::extensions::khr::GetPhysicalDeviceProperties2::name(),
        ]);

        // The InstanceCreateInfo needs the layer and extension names as a *const c_char slice.
        let enabled_extensions = {
            let supported_extension_properties =
                entry.enumerate_instance_extension_properties(None)?;

            let mut missing_any_extensions = false;
            for required_extension in required_extensions.iter() {
                if !supported_extension_properties
                    .iter()
                    .map(|supported_extension| unsafe {
                        CStr::from_ptr(supported_extension.extension_name.as_ptr())
                    })
                    .any(|supported_extension| supported_extension == *required_extension)
                {
                    log::error!(
                        "The required instance extension {} is unsupported.",
                        required_extension.to_str().unwrap()
                    );
                    missing_any_extensions = true;
                }
            }

            if missing_any_extensions {
                return Err("One or more required instance extensions are unsupported. Initialisation can not continue.".to_string().into());
            }

            required_extensions
                .iter()
                .map(|extension| extension.as_ptr())
                .collect::<Vec<_>>()
        };

        let mut debug_messenger_info = ash::vk::DebugUtilsMessengerCreateInfoEXT::builder()
            .flags(ash::vk::DebugUtilsMessengerCreateFlagsEXT::empty())
            .message_severity(
                ash::vk::DebugUtilsMessageSeverityFlagsEXT::WARNING
                    | ash::vk::DebugUtilsMessageSeverityFlagsEXT::ERROR,
            )
            .message_type(
                ash::vk::DebugUtilsMessageTypeFlagsEXT::GENERAL
                    | ash::vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION
                    | ash::vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE,
            )
            .pfn_user_callback(Some(debug_callback))
            .user_data(std::ptr::null_mut());

        let instance_info = ash::vk::InstanceCreateInfo::builder()
            .push_next(&mut debug_messenger_info)
            .application_info(&application_info)
            .enabled_layer_names(&enabled_layers)
            .enabled_extension_names(&enabled_extensions);

        let instance = unsafe { entry.create_instance(&instance_info, None)? };

        let (debug_utils_loader, debug_utils_messenger) = if cfg!(debug_assertions) {
            let debug_utils_loader = ash::extensions::ext::DebugUtils::new(&entry, &instance);
            let debug_utils_messenger = unsafe {
                debug_utils_loader.create_debug_utils_messenger(&debug_messenger_info, None)?
            };

            (Some(debug_utils_loader), Some(debug_utils_messenger))
        } else {
            (None, None)
        };

        Ok(Self {
            entry: entry,
            instance_handle: instance,

            debug_utils_loader: debug_utils_loader,
            debug_utils_messenger: debug_utils_messenger,
        })
    }
}

impl Drop for Instance {
    fn drop(&mut self) {
        unsafe {
            if self.debug_utils_loader.is_some() {
                self.debug_utils_loader
                    .as_ref()
                    .unwrap()
                    .destroy_debug_utils_messenger(self.debug_utils_messenger.unwrap(), None);
            }

            self.instance_handle.destroy_instance(None);
        }
    }
}

// Custom callback for the validation layers.
unsafe extern "system" fn debug_callback(
    message_flag: ash::vk::DebugUtilsMessageSeverityFlagsEXT,
    message_type: ash::vk::DebugUtilsMessageTypeFlagsEXT,
    p_callback_data: *const ash::vk::DebugUtilsMessengerCallbackDataEXT,
    _: *mut c_void,
) -> ash::vk::Bool32 {
    use ash::vk::DebugUtilsMessageSeverityFlagsEXT as Flag;

    let message = CStr::from_ptr((*p_callback_data).p_message);
    match message_flag {
        Flag::INFO => log::info!("{:?} - {:?}", message_type, message),
        Flag::WARNING => log::warn!("{:?} - {:?}", message_type, message),
        Flag::ERROR => log::error!("{:?} - {:?}", message_type, message),
        _ => (),
    }

    ash::vk::FALSE
}
