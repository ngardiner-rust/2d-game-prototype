use std::{
    error::Error,
    ops::Deref,
    sync::{Arc, Mutex},
};

use winit::window::Window;

use super::{Device, FrameResources, Instance, Surface, Swapchain};

const MAX_FRAMES_IN_FLIGHT: usize = 2;

pub struct Renderer {
    pub frame_index: usize,
    pub dynamic_rendering: ash::extensions::khr::DynamicRendering,
    pub frames_in_flight: Vec<FrameResources>,
    pub swapchain: Swapchain,
    pub device: Arc<Mutex<Device>>,
    pub surface: Surface,
    pub instance: Instance,
}

impl Renderer {
    pub fn init(window: &Window) -> Result<Self, Box<dyn Error>> {
        let instance = Instance::new(window)?;

        let surface = Surface::new(window, &instance)?;

        let device = Arc::new(Mutex::new(Device::new(&instance, &surface)?));

        let swapchain = Swapchain::new(window, &instance, &surface, device.clone(), None)?;

        let mut frames_in_flight: Vec<FrameResources> = vec![];
        for _i in 0..MAX_FRAMES_IN_FLIGHT {
            frames_in_flight.push(FrameResources::new(device.clone())?);
        }

        let dynamic_rendering = ash::extensions::khr::DynamicRendering::new(
            &instance.instance_handle,
            &device.lock().unwrap().device_handle,
        );

        Ok(Self {
            instance: instance,
            surface: surface,
            device: device,
            swapchain: swapchain,
            frames_in_flight: frames_in_flight,
            dynamic_rendering: dynamic_rendering,
            frame_index: 0,
        })
    }

    pub fn get_frame(&self) -> &FrameResources {
        let index = self.frame_index % MAX_FRAMES_IN_FLIGHT;
        &self.frames_in_flight[index]
    }

    pub fn draw_frame(&mut self) -> Result<(), Box<dyn Error>> {
        unsafe {
            self.get_frame().await_render_finished_fence()?;
            self.get_frame().reset_render_finished_fence()?;

            self.get_frame().reset_command_buffer()?;

            let (image_index, result) = self
                .swapchain
                .swapchain_loader
                .acquire_next_image(
                    self.swapchain.swapchain_handle,
                    std::u64::MAX,
                    self.get_frame().image_acquired_semaphore,
                    ash::vk::Fence::null(),
                )
                .unwrap();

            self.get_frame()
                .begin_command_buffer(ash::vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT)?;

            let image_memory_barriers = [
                ash::vk::ImageMemoryBarrier2::builder()
                    .src_stage_mask(ash::vk::PipelineStageFlags2::TOP_OF_PIPE)
                    .src_access_mask(ash::vk::AccessFlags2::empty())
                    .dst_stage_mask(ash::vk::PipelineStageFlags2::COLOR_ATTACHMENT_OUTPUT)
                    .dst_access_mask(
                        ash::vk::AccessFlags2::COLOR_ATTACHMENT_WRITE
                            | ash::vk::AccessFlags2::COLOR_ATTACHMENT_READ,
                    )
                    .old_layout(ash::vk::ImageLayout::UNDEFINED)
                    .new_layout(ash::vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                    .src_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED)
                    .dst_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED)
                    .image(self.swapchain.images[image_index as usize])
                    .subresource_range(
                        ash::vk::ImageSubresourceRange::builder()
                            .aspect_mask(ash::vk::ImageAspectFlags::COLOR)
                            .base_mip_level(0)
                            .level_count(1)
                            .base_array_layer(0)
                            .layer_count(1)
                            .build(),
                    )
                    .build(),
                ash::vk::ImageMemoryBarrier2::builder()
                    .src_stage_mask(ash::vk::PipelineStageFlags2::COLOR_ATTACHMENT_OUTPUT)
                    .src_access_mask(ash::vk::AccessFlags2::COLOR_ATTACHMENT_WRITE)
                    .dst_stage_mask(ash::vk::PipelineStageFlags2::BOTTOM_OF_PIPE)
                    .dst_access_mask(ash::vk::AccessFlags2::empty())
                    .old_layout(ash::vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                    .new_layout(ash::vk::ImageLayout::PRESENT_SRC_KHR)
                    .src_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED)
                    .dst_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED)
                    .image(self.swapchain.images[image_index as usize])
                    .subresource_range(
                        ash::vk::ImageSubresourceRange::builder()
                            .aspect_mask(ash::vk::ImageAspectFlags::COLOR)
                            .base_mip_level(0)
                            .level_count(1)
                            .base_array_layer(0)
                            .layer_count(1)
                            .build(),
                    )
                    .build(),
            ];

            let dependency_info = ash::vk::DependencyInfo::builder()
                .dependency_flags(ash::vk::DependencyFlags::empty())
                .memory_barriers(&[])
                .buffer_memory_barriers(&[])
                .image_memory_barriers(&image_memory_barriers);

            self.device
                .lock()
                .unwrap()
                .device_handle
                .cmd_pipeline_barrier2(self.get_frame().command_buffer, &dependency_info);

            let colour_attachment_rendering_info = ash::vk::RenderingAttachmentInfoKHR::builder()
                .image_view(self.swapchain.image_views[image_index as usize])
                .image_layout(ash::vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                .resolve_mode(ash::vk::ResolveModeFlags::NONE)
                .load_op(ash::vk::AttachmentLoadOp::CLEAR)
                .store_op(ash::vk::AttachmentStoreOp::STORE)
                .clear_value(ash::vk::ClearValue {
                    color: ash::vk::ClearColorValue {
                        float32: [0.1, 0.1, 0.1, 1.0],
                    },
                })
                .build();

            let render_area = ash::vk::Rect2D::builder()
                .extent(self.swapchain.extent)
                .offset(*ash::vk::Offset2D::builder().x(0).y(0))
                .build();

            let rendering_info = ash::vk::RenderingInfoKHR::builder()
                .flags(ash::vk::RenderingFlagsKHR::empty())
                .render_area(render_area)
                .layer_count(0)
                .view_mask(0)
                .color_attachments(&[colour_attachment_rendering_info])
                .build();

            self.dynamic_rendering
                .cmd_begin_rendering(self.get_frame().command_buffer, &rendering_info);

            self.dynamic_rendering
                .cmd_end_rendering(self.get_frame().command_buffer);

            self.get_frame().end_command_buffer()?;

            let submit_info = ash::vk::SubmitInfo::builder()
                .wait_semaphores(&[self.get_frame().image_acquired_semaphore])
                .wait_dst_stage_mask(&[ash::vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT])
                .command_buffers(&[self.get_frame().command_buffer])
                .signal_semaphores(&[self.get_frame().render_finished_semaphore])
                .build();

            let presentation_queue = self.device.lock().unwrap().graphics_queue;

            self.device
                .lock()
                .unwrap()
                .device_handle
                .queue_submit(
                    presentation_queue,
                    &[submit_info],
                    self.get_frame().render_finished_fence,
                )
                .unwrap();

            let swapchains = [self.swapchain.swapchain_handle];

            let image_indices = [image_index];
            let present_info = ash::vk::PresentInfoKHR::builder()
                .wait_semaphores(&[self.get_frame().render_finished_semaphore])
                .swapchains(&swapchains)
                .image_indices(&image_indices)
                .build();

            // TODO: Recreate swapchain when this occurs.
            let suboptimal = self
                .swapchain
                .swapchain_loader
                .queue_present(presentation_queue, &present_info)
                .unwrap();
        }

        self.frame_index += 1;

        Ok(())
    }
}
