use std::error::Error;

use winit::window::Window;

use super::Instance;

pub struct Surface {
    pub surface_loader: ash::extensions::khr::Surface,
    pub surface_handle: ash::vk::SurfaceKHR,
}

impl Surface {
    pub fn new(window: &Window, instance: &Instance) -> Result<Self, Box<dyn Error>> {
        let surface_loader =
            ash::extensions::khr::Surface::new(&instance.entry, &instance.instance_handle);

        let surface = unsafe {
            ash_window::create_surface(&instance.entry, &instance.instance_handle, window, None)?
        };

        Ok(Self {
            surface_loader: surface_loader,
            surface_handle: surface,
        })
    }
}

impl Drop for Surface {
    fn drop(&mut self) {
        unsafe {
            self.surface_loader
                .destroy_surface(self.surface_handle, None);
        }
    }
}
