use std::{
    error::Error,
    sync::{Arc, Mutex},
};

use winit::window::Window;

use super::{Device, Instance, Surface};

pub struct Swapchain {
    pub swapchain_loader: ash::extensions::khr::Swapchain,
    pub swapchain_handle: ash::vk::SwapchainKHR,

    pub image_format: ash::vk::SurfaceFormatKHR,
    pub extent: ash::vk::Extent2D,

    pub images: Vec<ash::vk::Image>,
    pub image_views: Vec<ash::vk::ImageView>,

    device: Arc<Mutex<Device>>,
}

impl Swapchain {
    pub fn new(
        window: &Window,
        instance: &Instance,
        surface: &Surface,
        device: Arc<Mutex<Device>>,
        old_swapchain: Option<&Swapchain>,
    ) -> Result<Self, Box<dyn Error>> {
        let swapchain_loader = ash::extensions::khr::Swapchain::new(
            &instance.instance_handle,
            &device.lock().unwrap().device_handle,
        );

        let supported_surface_formats = unsafe {
            surface.surface_loader.get_physical_device_surface_formats(
                device.lock().unwrap().physical_device,
                surface.surface_handle,
            )?
        };

        let swapchain_image_format = supported_surface_formats
            .iter()
            .cloned()
            .find(|format| {
                format.format == ash::vk::Format::B8G8R8A8_SRGB
                    && format.color_space == ash::vk::ColorSpaceKHR::SRGB_NONLINEAR
            })
            .unwrap_or_else(|| supported_surface_formats[0]);

        let supported_surface_capabilities = unsafe {
            surface
                .surface_loader
                .get_physical_device_surface_capabilities(
                    device.lock().unwrap().physical_device,
                    surface.surface_handle,
                )?
        };

        let swapchain_extent = match supported_surface_capabilities.current_extent.width {
            std::u32::MAX => ash::vk::Extent2D::builder()
                .width(window.inner_size().width.clamp(
                    supported_surface_capabilities.min_image_extent.width,
                    supported_surface_capabilities.max_image_extent.width,
                ))
                .height(window.inner_size().height.clamp(
                    supported_surface_capabilities.min_image_extent.height,
                    supported_surface_capabilities.max_image_extent.height,
                ))
                .build(),
            _ => supported_surface_capabilities.current_extent,
        };

        let swapchain_image_count = if supported_surface_capabilities.max_image_count > 0
            && supported_surface_capabilities.min_image_count + 1
                > supported_surface_capabilities.max_image_count
        {
            supported_surface_capabilities.max_image_count
        } else {
            supported_surface_capabilities.min_image_count + 1
        };

        let swapchain_present_mode = unsafe {
            surface
                .surface_loader
                .get_physical_device_surface_present_modes(
                    device.lock().unwrap().physical_device,
                    surface.surface_handle,
                )?
                .into_iter()
                .find(|&mode| mode == ash::vk::PresentModeKHR::FIFO)
                .unwrap_or(ash::vk::PresentModeKHR::FIFO)
        };

        let queue_family_indices = [device.lock().unwrap().graphics_family_index];

        let swapchain_info = ash::vk::SwapchainCreateInfoKHR::builder()
            .flags(ash::vk::SwapchainCreateFlagsKHR::empty())
            .surface(surface.surface_handle)
            .min_image_count(swapchain_image_count)
            .image_format(swapchain_image_format.format)
            .image_color_space(swapchain_image_format.color_space)
            .image_extent(swapchain_extent)
            .image_array_layers(1)
            .image_usage(ash::vk::ImageUsageFlags::COLOR_ATTACHMENT)
            .image_sharing_mode(ash::vk::SharingMode::EXCLUSIVE)
            .queue_family_indices(&queue_family_indices)
            .pre_transform(supported_surface_capabilities.current_transform)
            .composite_alpha(ash::vk::CompositeAlphaFlagsKHR::OPAQUE)
            .present_mode(swapchain_present_mode)
            .clipped(true)
            .old_swapchain(
                old_swapchain.map_or(ash::vk::SwapchainKHR::null(), |swapchain| {
                    swapchain.swapchain_handle
                }),
            );

        let swapchain = unsafe { swapchain_loader.create_swapchain(&swapchain_info, None)? };

        let swapchain_images = unsafe { swapchain_loader.get_swapchain_images(swapchain)? };

        let mut swapchain_image_views: Vec<ash::vk::ImageView> = Vec::new();
        for swapchain_image in swapchain_images.iter() {
            let image_view_info = ash::vk::ImageViewCreateInfo::builder()
                .image(*swapchain_image)
                .view_type(ash::vk::ImageViewType::TYPE_2D)
                .format(swapchain_image_format.format)
                .components(
                    ash::vk::ComponentMapping::builder()
                        .r(ash::vk::ComponentSwizzle::IDENTITY)
                        .g(ash::vk::ComponentSwizzle::IDENTITY)
                        .b(ash::vk::ComponentSwizzle::IDENTITY)
                        .a(ash::vk::ComponentSwizzle::IDENTITY)
                        .build(),
                )
                .subresource_range(
                    ash::vk::ImageSubresourceRange::builder()
                        .aspect_mask(ash::vk::ImageAspectFlags::COLOR)
                        .base_mip_level(0)
                        .level_count(1)
                        .base_array_layer(0)
                        .layer_count(1)
                        .build(),
                );

            let image_view = unsafe {
                device
                    .lock()
                    .unwrap()
                    .device_handle
                    .create_image_view(&image_view_info, None)?
            };

            swapchain_image_views.push(image_view);
        }

        Ok(Self {
            swapchain_loader: swapchain_loader,
            swapchain_handle: swapchain,

            image_format: swapchain_image_format,
            extent: swapchain_extent,

            images: swapchain_images,
            image_views: swapchain_image_views,

            device: device,
        })
    }
}

impl Drop for Swapchain {
    fn drop(&mut self) {
        unsafe {
            for &swapchain_image_view in self.image_views.iter() {
                self.device
                    .lock()
                    .unwrap()
                    .device_handle
                    .destroy_image_view(swapchain_image_view, None);
            }

            self.swapchain_loader
                .destroy_swapchain(self.swapchain_handle, None);
        }
    }
}
